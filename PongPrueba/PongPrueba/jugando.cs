﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace PongPrueba
{
    public partial class jugando : Form
    {
        public jugando()
        {
            InitializeComponent();
        }

        int velocidad = 5;
        int contador = 0;
        int puntaje = 5;

        bool arriba;
        bool izquierda;


        private void timer1_Tick(object sender, EventArgs e)
        {

            if (pictureBox1.Left > pictureBox2.Left)
            {
                timer1.Enabled = false;
                MessageBox.Show("Puntaje: " + puntaje.ToString() + "veces");
                puntaje = 0;
                velocidad = 5;
                contador = 0;

            }

            if (pictureBox1.Left + pictureBox1.Width >= pictureBox2.Left &&
                pictureBox1.Left + pictureBox1.Width <= pictureBox2.Left + pictureBox2.Width &&
                pictureBox1.Top + pictureBox1.Height >= pictureBox2.Top &&
                pictureBox1.Top + pictureBox1.Height <= pictureBox2.Top + pictureBox2.Height)
            {
                izquierda = false;
                puntaje += 1;
                this.Text = "Puntaje" + puntaje.ToString() + "";
                contador += 1;
                if (contador > 2)
                {
                    velocidad += 1;
                    contador = 0;
                }


            }


            // movimiento de la pelota

            if(izquierda)
            {
                pictureBox1.Left += velocidad; //derecha
            }
            else
            {
                pictureBox1.Left -= velocidad; // izquierda
            }

            if (arriba)
            {
                pictureBox1.Top += velocidad; // abajo
            }
            else 
            {
                pictureBox1.Top -= velocidad; // arriba
            }

            if (pictureBox1.Top >= this.Height - 50) // se pega en la pared de abajo
            {
                arriba = false;
            }
            if (pictureBox1.Top <= 0) // se pega en la pared de arriba
            {
                arriba = true;
            }
            if (pictureBox1.Left <= 0) // se pega en la pared de izquierda
            {
                izquierda = true;
            }

     


        }

        private void jugando_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner.Show();

        }

        private void jugando_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox2.Top = e.Y;
        }

        private void jugando_Load(object sender, EventArgs e)
        {
            this.Text = "Puntaje: 0";
            Random aleatorio = new Random();
            pictureBox1.Location = new Point(0,aleatorio.Next(this.Height));
            arriba = true;
            izquierda = true;
            timer1.Enabled = true;
            puntaje = 0;




        }



    }
}
