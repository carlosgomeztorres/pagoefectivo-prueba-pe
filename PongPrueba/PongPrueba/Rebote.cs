﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PongPrueba
{
    public partial class Rebote : Form
    {
        public Rebote()
        {
            InitializeComponent();
        }

        bool arriba;
        bool izquierda;
        int velocidad = 5;
        int contador = 0;
        int velocidadizq = 5;
    
        private void timer1_Tick(object sender, EventArgs e)
        {

            if (arriba)
            {
                pictureBox1.Top += velocidad; // abajo
            }
            else
            {
                pictureBox1.Top -= velocidad; // arriba
            }

            //if (pictureBox1.Top >= 160) // se pega en la pared de abajo
            if (pictureBox1.Top >= this.Height - 68) // se pega en la pared de abajo
            {
                arriba = false;
                timer1.Enabled = false;
            }
            if (pictureBox1.Top <= 70) // se pega en la pared de arriba
            {
                arriba = true;
            }
        }

        private void Rebote_Load(object sender, EventArgs e)
        {

          izquierda = true;

        }

        private void Rebote_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer2.Enabled = true;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            if ((pictureBox2.Left - velocidadizq) == pictureBox1.Left && pictureBox1.Top >= this.Height - 68)
            {
                timer1.Enabled = false;
                timer2.Enabled = false;
                MessageBox.Show("Puntaje: " + "0" + "veces");

            }

            if (izquierda)
            {
                pictureBox2.Left += velocidadizq; //derecha
            }
            else
            {
                pictureBox2.Left -= velocidadizq; // izquierda
            }

            if (pictureBox2.Left <= 0) // se pega en la pared de izquierda
            {
                izquierda = true;
            }
            if (pictureBox2.Left + pictureBox2.Width >= this.Width) // se pega en la pared de derecha
            {
                izquierda = false;

                contador += 1;

                if (contador > 3)
                {
                    velocidadizq += 1;
                }
            }

        }

        private void Rebote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                timer1.Enabled = true;
            }
        }
    }
}
